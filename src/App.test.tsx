import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders text', () => {
  const { getByText } = render(<App />);
  const element = getByText(/You did It!/i);
  expect(element).toBeInTheDocument();
});

test('renders image', () => {
  const { getByTestId } = render(<App />);
  const element = getByTestId("rocket");
  expect(element).toBeInTheDocument();
});
